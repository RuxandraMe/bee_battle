let beeConfig = {
    queen: {
        hp: 100,
        damage: 8
    },
    worker: {
        hp: 75,
        damage: 10
    },
    drone: {
        hp: 50,
        damage: 12
    },
    hive: {
        queens: 1,
        workers: 5,
        drones: 8
    }
}

class Bee {
    constructor(beeId, beeType, health, damage) {
        this._beeId = beeId;
        this._beeType = beeType;
        this._beeHealth = health;
        this._damage = damage;
    }
}

let beeList = new Array();
let playerName = ''
let isRestart = false;

function start() {

    if (playerName.length == 0) {
        playerName = document.getElementById('new_player_field').value;
        if (playerName.trim().length == 0) {
            alert('Please enter a player name!');
            document.getElementById('new_player_field').focus();
            return;
        }
    }

    document.getElementById('player_name_p').innerHTML = 'Player: ' + playerName;
    document.getElementById('new_player').style.display = 'none';

    initializeBeeHive();
    if (isRestart) {
        redrawHiveStats();
    } else {
        drawHiveStats();
    }
    isRestart = false;

    document.getElementById('btn_hit').innerHTML = 'HIT'
    document.getElementById('btn_hit').onclick = function () { attack(); };
}

function attack() {
    animateBees();

    let defenderIndex = Math.floor(Math.random() * beeList.length);

    while (beeList[defenderIndex]._beeHealth <= 0) {
        defenderIndex = Math.floor(Math.random() * beeList.length);
    }

    beeList[defenderIndex]._beeHealth -= beeList[defenderIndex]._damage;

    redrawHiveStats();
    drawTurnsHistory(beeList[defenderIndex]._beeId,beeList[defenderIndex]._beeType, beeList[defenderIndex]._damage);

    if (checkEndGame()) {
        document.getElementById('myModal').style.display = 'block';
    }
}

function checkEndGame() {
    let deadCounts = 0;
    let queenDead = false;
    beeList.forEach(element => {
        if ('queen' == element._beeType && element._beeHealth <= 0) {
            queenDead = true;
        }
        else if (element._beeHealth <= 0) {
            deadCounts++;
        }
    });

    return queenDead || deadCounts == beeList.length;
}

function initializeBeeHive() {

    beeList = new Array();
    let idVar = 0;
    for (var i = 0; i < beeConfig.hive.queens; i++) {
        beeList.push(new Bee('bee' + idVar, 'queen', beeConfig.queen.hp, beeConfig.queen.damage));
        idVar++;
    }
    for (var i = 0; i < beeConfig.hive.workers; i++) {
        beeList.push(new Bee('bee' + idVar, 'worker', beeConfig.worker.hp, beeConfig.worker.damage));
        idVar++;
    }
    for (var i = 0; i < beeConfig.hive.drones; i++) {
        beeList.push(new Bee('bee' + idVar, 'drone', beeConfig.drone.hp, beeConfig.drone.damage));
        idVar++;
    }
}

function drawTurnsHistory(defenderId,defenderType, dealtDamage) {

    let htmlElementText = "<div>"
        + "<span class=\"bee_type\">" + defenderType + " </span>"
        + "<span> (" + defenderId + ") </span>"
        + "<span class=\"fa fa-arrow-circle-right\"></span>"
        + "<span class=\"dealt_damage\"> (" + dealtDamage + ")</span>"
        + "</div>"


    let historySection = document.getElementById('history_section');
    historySection.innerHTML += htmlElementText;
    historySection.scrollTop = historySection.scrollHeight;
}

function drawHiveStats() {

    document.getElementById('status_section').innerHTML = "";
    beeList.forEach(element => {
        let htmlElementText = "<div>" +
            "<span class=\"fa fa-forumbee\" style=\"color: #ffbf00;\"></span>" +
            "<span> " + element._beeId + " - </span><span class=\"bee_type\">" + element._beeType + "</span>" +
            "<canvas id=\"stat" + element._beeId + "\" width=\"70px\" height=\"17px\"></canvas>&nbsp;" +
            "<span id=\"health" + element._beeId + "\"></span>" +
            "</div>";

        let statusSection = document.getElementById('status_section');
        statusSection.innerHTML += htmlElementText;
    });

    redrawHiveStats();
}

function redrawHiveStats() {
    beeList.forEach(element => {
        var canvasHP = document.getElementById('stat' + element._beeId).getContext('2d');
        drawHealthBar(canvasHP, 5, 5, 70, 17, element._beeHealth, beeConfig[element._beeType].hp);
        document.getElementById('health' + element._beeId).innerHTML = (element._beeHealth >= 0 ? element._beeHealth : 0) + 'hp';
    });
}


function drawHealthBar(canvas, x, y, width, height, health, max_health) {

    if (health > max_health) { health = max_health; }
    if (health < 0) { health = 0; }

    canvas.fillStyle = '#000000';
    canvas.fillRect(x, y, width, height);

    if (health < 66 * max_health / 100) {
        canvas.fillStyle = '#f5a340';
    }
    else {
        canvas.fillStyle = '#00ff00';
    }
    if (health < 33 * max_health / 100) {
        canvas.fillStyle = '#ff0000';
    }

    canvas.fillRect(x + 1, y + 1, (health / max_health) * (width - 2), height - 2);
}

let img = document.createElement("img");
let img2 = document.createElement("img");
let src = document.getElementById("attackerBee");
let src2 = document.getElementById("defenderBee");
img.src = "images/1.png";
img2.src = "images/2.png";
img.style.width = "100px";
img2.style.width = "100px";
src.appendChild(img);
src2.appendChild(img2);

function animateBees() {
    let elem = document.getElementById("attackerBee");
    let elem2 = document.getElementById("defenderBee");
    let pos = 0;
    let id = setInterval(frame, 0);
    img2.src = "images/2.png";


    function frame() {
        if (pos == 270) {
            clearInterval(id);
            img2.src = "images/5.png";
        } else {
            pos++;
            elem.style.left = pos + 'px';
            elem2.style.right = pos + 'px';
        }
    }
}


function playAgain() {
    document.getElementById('myModal').style.display = 'none';
    isRestart = true;
    document.getElementById('history_section').innerHTML = "";
    start();
}