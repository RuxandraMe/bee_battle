Bee battle game

A mini game where some bees fighting each other, developed in ES6, HTML5 and CSS3.

How to play:

You are required to enter a player name and press start. The game will initialize a hive of bees with 1 queen, 5 Workers and 8 Drones.
By pressing HIT button a random bee will attack another bee giving damage to it.
Each type of bee has an HP (health points) and a damage configured.
The game ends when there is only one bee alive, or when the queen is dead.

